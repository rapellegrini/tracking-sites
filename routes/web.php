<?php

use Illuminate\Support\Facades\Route;

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::group(['prefix' => 'request-history'], function () {
    Route::get('checkstatus', ['as' => 'request-history.checkstatus', 'uses' => 'RequestHistoryController@checkStatus']);
    Route::get('index/{id}', ['as' => 'request-history.index', 'uses' => 'RequestHistoryController@index']);
});

Route::group(['prefix' => 'url'], function () {
    Route::get('index', ['as' => 'url.index', 'uses' => 'UrlController@index']);
    Route::get('create', ['as' => 'url.create', 'uses' => 'UrlController@create']);
    Route::post('store', ['as' => 'url.store', 'uses' => 'UrlController@store']);
    Route::get('history/requets/{id}', ['as' => 'urls.history.request', 'uses' => 'UrlController@historyRequest']);
});

