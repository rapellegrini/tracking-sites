<!DOCTYPE html>
<html lang="en">
@section('htmlheader')
    @include('layouts.partials.htmlheader')
@show

<body>
@include('layouts.partials.menu')
@include('layouts.partials.navbar', ["title" => $title ?? ""])

@yield('main-content')

@section('scripts')
    @include('layouts.partials.scripts')
    @include('layouts.partials.helpers-js')

    @parent
@show

</body>
</html>
