<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
	<meta name="description" content="">
	<meta name="author" content="">

	<title>Tracking Sites</title>


	<!-- Main Styles -->
    <link href="{{ asset('assets/styles/style.css') }}" rel="stylesheet" type="text/css" />

	<!-- mCustomScrollbar -->
	 <link href="{{ asset('assets/plugin/mCustomScrollbar/jquery.mCustomScrollbar.min.css') }}" rel="stylesheet" type="text/css" />


	<!-- Waves Effect -->
	 <link href="{{ asset('assets/plugin/waves/waves.min.css') }}" rel="stylesheet" type="text/css" />

	<!-- Sweet Alert -->
	 <link href="{{ asset('assets/plugin/sweet-alert/sweetalert.css') }}" rel="stylesheet" type="text/css" />

    <!-- Timepicker -->
    <link href="{{ asset('assets/plugin/timepicker/bootstrap-timepicker.min.css') }}" rel="stylesheet" type="text/css" />

    <!-- Datepicker -->
    <link href="{{ asset('assets/plugin/datepicker/css/bootstrap-datepicker.css') }}" rel="stylesheet" type="text/css" />

    <!-- Touch Spin -->
    <link href="{{ asset('assets/plugin/touchspin/jquery.bootstrap-touchspin.min.css') }}" rel="stylesheet" type="text/css" />

    <!-- Remodal -->
    <link href="{{ asset('assets/plugin/modal/remodal/remodal.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/plugin/modal/remodal/remodal-default-theme.css') }}" rel="stylesheet" type="text/css" />

    <link href="{{ asset('assets/color-switcher/color-switcher.min.css') }}" rel="stylesheet" type="text/css" />


    <link href="https://fonts.googleapis.com/css?family=Ubuntu&display=swap" rel="stylesheet">

    <!-- Select2 -->
    <link href="{{ asset('select2/dist/css/select2.min.css') }}" rel="stylesheet">

    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

    <style type="text/css">
    .ui-autocomplete{
       z-index: 1050 !important;
    }
    </style>

</head>
