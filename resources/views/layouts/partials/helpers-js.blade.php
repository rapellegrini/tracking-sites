<script type="text/javascript">
    $(document).ready(function () {
    });

    function dateToBR(date)
    {	
        return date.split('-').reverse().join('/');
    }

    function dateToEN(date)
    {	
        return date.split('/').reverse().join('-');
    }

    function formatMoneyToBR(n) 
    {
        return n.toLocaleString("pt-BR", { style: "currency" , currency:"BRL"});
    }

</script>
