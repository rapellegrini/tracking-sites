<div class="main-menu">
    <header class="header">
        <a href="index.html" class="logo">Tracking Sites
        </a>
        <button type="button" class="button-close fa fa-times js__menu_close"></button>
        <div class="user">
            <a href="#" class="avatar"><img src="http://placehold.it/80x80" alt=""><span
                    class="status online"></span></a>
            <h5 class="name"><a href="profile.html">{{-- Auth::user()->name --}}</a></h5>
            <h5 class="position">{{ Auth::user()->name }}</h5>

        </div>
    </header>
    <!-- /.header -->
    <div class="content">

        <div class="navigation">
            <h5 class="title">Menu</h5>
            <!-- /.title -->
            <ul class="menu js__accordion">

                <li class="current">
                    <a class="waves-effect" href="#"><i class="menu-icon fa fa-home"></i><span>Principal</span></a>
                </li>

                <li>
                    <a class="waves-effect parent-item js__control" href="#"><i
                            class="menu-icon fa fa-rocket"></i><span>Urls</span><span
                            class="menu-arrow fa fa-angle-down"></span></a>
                    <ul class="sub-menu js__content">
                        <li><a href="{{ route('url.index') }}">Ver Urls</a></li>
                    </ul>
                    <!-- /.sub-menu js__content -->
                </li>
            </ul>
        </div>
        <!-- /.navigation -->
    </div>
    <!-- /.content -->
</div>
<!-- /.main-menu -->
