<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
<script src="assets/script/html5shiv.min.js"></script>
<script src="assets/script/respond.min.js"></script>
<![endif]-->
<!--
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="{{ asset('assets/scripts/jquery.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/scripts/modernizr.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/plugin/bootstrap/js/bootstrap.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/plugin/mCustomScrollbar/jquery.mCustomScrollbar.concat.min.js') }}"
        type="text/javascript"></script>
<script src="{{ asset('assets/plugin/nprogress/nprogress.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/plugin/sweet-alert/sweetalert.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/plugin/waves/waves.min.js') }}" type="text/javascript"></script>

<!-- Full Screen Plugin -->
<script src="{{ asset('assets/plugin/fullscreen/jquery.fullscreen-min.js') }}" type="text/javascript"></script>

<!-- Timepicker -->
<script src="{{ asset('assets/plugin/timepicker/bootstrap-timepicker.min.js') }}" type="text/javascript"></script>

<!-- Datepicker -->
<script src="{{ asset('assets/plugin/datepicker/js/bootstrap-datepicker.js') }}" type="text/javascript"></script>

<!-- Remodal -->
<script src="{{ asset('assets/plugin/modal/remodal/remodal.min.js') }}" type="text/javascript"></script>

<!-- Touch Spin -->
<script src="{{ asset('assets/plugin/touchspin/jquery.bootstrap-touchspin.min.js') }}" type="text/javascript"></script>


<script src="{{ asset('assets/scripts/main.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/color-switcher/color-switcher.min.js') }}" type="text/javascript"></script>

<!-- Select2 -->
<script src="{{ asset('select2/dist/js/select2.min.js') }}" defer></script>


<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.15/jquery.mask.min.js " type="text/javascript"></script>

<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

