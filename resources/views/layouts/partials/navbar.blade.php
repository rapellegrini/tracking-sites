<div class="fixed-navbar">
        <div class="pull-left">
            <button type="button" class="menu-mobile-button glyphicon glyphicon-menu-hamburger js__menu_mobile"></button>
            <h1 class="page-title"></h1>
        </div>
        <div class="pull-right">
            <a href="{{ route('url.create') }}" type="button" class="btn btn-success waves-effect waves-light"
               ><i class="ico ico-left fa fa-check"></i>Cadastrar nova URL</a>
            <a a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();" {{ __('Logout') }} class="ico-item fa fa-power-off"></a>
        </div>
        <!-- /.pull-right -->

    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
        @csrf
    </form>
</div>
    <!-- /.fixed-navbar -->
