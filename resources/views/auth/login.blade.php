<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>tracking-sites</title>
    <link href="{{ asset('assets/styles/style.css') }}" rel="stylesheet" type="text/css"/>

    <!-- Waves Effect -->
    <link href="{{ asset('assets/plugin/waves/waves.min.css') }}" rel="stylesheet" type="text/css"/>

</head>

<body>

<div id="">
    <div class="errors-msg alert alert-danger" style="display: none"></div>
    <div class="card-content">
        @if( isset($errors) && count($errors) > 0 )
            <div class="alert alert-danger">
                @foreach( $errors->all() as $error )
                    <p>{{$error}}</p>
                @endforeach
            </div>
        @endif
    </div>
    <form method="POST" action="{{ route('login') }}" class="frm-single">
        @csrf
        <div class="inside">
            <div class="title"><span>Tracking- Sites</span></div>
            <!-- /.title -->
            <div class="frm-title"></div>
            <!-- /.frm-title -->
            <div class="frm-input"><input id="email" name="email" type="email" placeholder="e-mail"
                                          class="frm-inp @error('email') is-invalid @enderror" value="{{ old('email') }}" required autocomplete="email" autofocus><i
                    class="fa fa-user frm-ico"></i></div>

            <!-- /.frm-input -->
            <div class="frm-input"><input id="password" name="password" type="password" placeholder="senha"
                                          class="frm-inp @error('password') is-invalid @enderror" required
                                          autocomplete="current-password"><i
                    class="fa fa-lock frm-ico"></i>
                @error('password')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                 </span>
                @enderror
            </div>

            <!-- /.frm-input -->
           
            <!-- /.clearfix -->
            <button type="submit" class="frm-submit">Entrar<i class="fa fa-arrow-circle-right"></i></button>
            
        </div>
        <!-- .inside -->
    </form>
    <!-- /.frm-single -->
</div><!--/#single-wrapper -->

<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
<script src="{{ asset('assets/script/html5shiv.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/script/respond.min.js') }}" type="text/javascript"></script>
<![endif]-->
<!--
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="assets/scripts/jquery.min.js"></script>
<script src="assets/scripts/modernizr.min.js"></script>
<script src="assets/plugin/bootstrap/js/bootstrap.min.js"></script>
<script src="assets/plugin/nprogress/nprogress.js"></script>
<script src="assets/plugin/waves/waves.min.js"></script>

<script src="assets/scripts/main.min.js"></script>
</body>
</html>
