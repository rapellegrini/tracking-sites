@extends('layouts.core')

@section('main-content')

    <div id="wrapper">
        <div class="main-content">
            <div class="row row-inline-block small-spacing">
                <div class="col-xs-12">
                    <div class="box-content">
                        <div>
                            <h4 style="display: inline-block" class="title-page">Minhas Urls</h4>

                            {{--<a href="{{ route('url.create') }}" type="button" class="btn btn-info btn-rounded"
                               style="float:right">Novo Motorista</a>--}}
                        </div>

                        <div class="errors-msg alert alert-danger" style="display: none"></div>
                        <div class="card-content">
                            @if( isset($errors) && count($errors) > 0 )
                                <div class="alert alert-danger">
                                    @foreach( $errors->all() as $error )
                                        <p>{{$error}}</p>
                                    @endforeach
                                </div>
                            @endif
                        </div>

                        <br>
                        @if (session('success'))
                            <div class="alert alert-success" style="padding-top: 15px;">
                                {{ session('success') }}
                            </div>
                        @endif
                        <div class="table-responsive">
                            <!--<button type="button" data-remodal-target="remodal" class="btn btn-primary waves-effect waves-light">Lauch Modal</button>-->
                            <table class="table table-bordered table-hover">
                                <thead>
                                <tr>
                                    <!--<th>#</th> -->
                                    <th style="color: #00BFFF;">Url</th>
                                    <th style="color: #00BFFF;">Ver Respostas</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                @forelse ($urls as $url)
                                    <!--<th scope="row">{{ $url->url }}</th> -->
                                        <td>{{ $url->url }}</td>
                                        <td>
                                         <button type="button" class="btn btn-light btn-xs waves-effect waves-light"
                                                    onclick="javascript:window.location.href ='{{ route('urls.history.request', ['id'=> $url->id] ) }}';">
                                                <i class="ico fa fa-external-link-square fa-2x"></i></button>&nbsp;
                                                
                                        </td>
                                </tr>
                                @empty
                                    <tr>
                                        <td colspan="6">Nenhum registro !</td>
                                    </tr>
                                @endforelse 
                                </tbody>
                            </table>
                        </div>
                        {{-- $motorists->links() --}} 
                    </div>
                    <!-- /.box-content -->
                </div>
            </div>
        </div>
    </div>

    <div class="remodal" id="remodal" data-remodal-id="remodal" role="dialog" aria-labelledby="modal1Title"
         aria-describedby="modal1Desc">
        <button data-remodal-action="close" class="remodal-close" aria-label="Close"></button>
        <div class="remodal-content">
            <h2 id="modal1Title">
                <Remoda></Remoda>
                Excluir Motorista?
            </h2>
            <p id="modal1Desc">
                Deseja excluir o motorista?
            </p>
        </div>
        <input type="hidden" id="motorist_id">
        <button data-remodal-action="cancel" id="btn-cancel" class="remodal-cancel">Cancelar</button>
        <button data-remodal-action="confirm" id="btn-delete-motorist" class="remodal-confirm">SIM</button>
    </div>

    <!-- ### preloader ### -->
    {{--@component('components.preloader')
    @endcomponent --}}

@endsection


@section('scripts')
    @parent
    <script type="text/javascript">
        $(document).ready(function () {

                });
        

        function apagar(id) {
            $('[data-remodal-id = remodal]').remodal().open();
            $("#motorist_id").val(id);
            // setar o ID do tamanho da caçamba, no elemnto input hidden do remodal
            // ou  verificar se é possivel usar localstorage do javascript

            //https://stackoverflow.com/questions/31808129/how-to-make-a-confirmation-modal-with-remodal
            //https://www.w3schools.com/bootstrap/bootstrap_ref_js_modal.asp
        }
    </script>
@endsection
