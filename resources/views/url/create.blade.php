@extends('layouts.core')

@section('main-content')
    <div id="wrapper">
        <div class="main-content">
            <div class="row small-spacing">

                <form action="{{ route('url.store') }}" method="POST">
                @csrf

                <div class="col-md-12">
                    <div class="box-content card white">
                        <div class="title-page-space-20">Nova URL</div>
                        <hr>
                        <!-- /.box-title -->
                
                        <div class="errors-msg alert alert-danger" style="display: none"></div>
                        <div class="card-content">
                            @if( isset($errors) && count($errors) > 0 )
                                <div class="alert alert-danger">
                                    @foreach( $errors->all() as $error )
                                        <p>{{$error}}</p>
                                    @endforeach
                                </div>
                        @endif

                        @if (session('success'))
                        <div class="alert alert-success" style="padding-top: 15px;">
                            {{ session('success') }}
                        </div>
                    @endif
                
                                
                        <!-- URL -->
                        <div class="form-group col-md-12" style="padding-bottom: 10px;">
                            <label for="name">Endereço URL</label>
                            <input type="text" class="form-control" name="url" id="url" placeholder="Digite a url do site">
                            {{--@if ($errors->has('name'))
                                <span class="help-block">
                                    {{ $errors->first('name') }}
                                </span>
                            @endif--}}
                        </div>
            
                        <!--space-->
                        <div style="padding-bottom: 5px;">&nbsp;</div>

                        <div id="here_table"></div>
            
                    </div>
                    <!-- /.card-content -->
                </div>
                <!-- /.box-content -->
                </div>

                <div class="col-md-12 text-right">
                    <button class="btn btn-success" id="btnCreateMotorist" type="submit" data-toggle="tooltip"><span>Salvar</span>
                    </button>
                <!--<button class="btn btn-danger btn-cancelar" type="button" data-toggle="tooltip" title="Cancelar alterações"> <span>{{ Lang::get('message.cancel') }}</span></button>
                    -->
                </div>

            </form>

            </div>
        </div>
    </div>

@endsection

@section('scripts')
    @parent
    <script type="text/javascript">
        $(document).ready(function () {

 
            //const createClock = setInterval(displayTime, 5000);
        });

        function displayTime() {

            // criar uma função que gera uma tabela dinamica com AJAX
            //alert('teste');
            $('#here_table').append(  '<table />' );

            for(i=0;i<3;i++){
            $('#here_table table').append( '<tr><td>' + 'result' +  i + '</td></tr>' );
            }
        }
    </script>
@endsection
