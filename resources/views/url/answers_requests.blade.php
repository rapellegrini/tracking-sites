@extends('layouts.core')

@section('main-content')

    <div id="wrapper">
        <div class="main-content">
            <input type="hidden" id="id" name="id" value="{{$id}}">
            <div class="row row-inline-block small-spacing">
                <div class="col-xs-12">
                    <div class="box-content">
                        <div>
                            <h4 style="display: inline-block" class="title-page">Respostas Requisições</h4>
                            <br><span>{{$url->url}}</span>
                        </div>

                        <div class="errors-msg alert alert-danger" style="display: none"></div>
                        <div class="card-content">
                            @if( isset($errors) && count($errors) > 0 )
                                <div class="alert alert-danger">
                                    @foreach( $errors->all() as $error )
                                        <p>{{$error}}</p>
                                    @endforeach
                                </div>
                            @endif
                        </div>

                        <br>
                        @if (session('success'))
                            <div class="alert alert-success" style="padding-top: 15px;">
                                {{ session('success') }}
                            </div>
                        @endif

                        <div class="table-responsive">
                            <div id="here_table"></div>
                        </div>
                      
                    </div>
                    <!-- /.box-content -->
                </div>
            </div>
        </div>
    </div>
@endsection


@section('scripts')
    @parent
    <script type="text/javascript">
          $(document).ready(function () {

 
        const createClock = setInterval(displayTime, 1000);
        });

        function displayTime() {

                var id = $("#id").val();
                
                var url = '{{ route("request-history.index", ":id") }}';
                url = url.replace(':id', id);
                $.ajax({
                    type: "GET",
                    data: {
                        "_token": "{{ csrf_token() }}"
                    },
                    url: url,
                    dataType: "html",
                    success: function (result) {
                        var resultado = JSON.parse(result);
                        $('#here_table').empty();
                        $.each(resultado.data, function (key, value) {
                            var html = "<tr>"+
                                "<td>STATUS CODE:&nbsp;" + value.status_code + " &nbsp;&nbsp; </td>"+
                                "<td>DATA CONSULTA:&nbsp;" + value.ts_query + "</td>"+
                                "</tr>";
                            $('#here_table').append(html);
                        });
                    },
                    beforeSend: function () {
                    },
                    complete: function (msg) {
                    }

                }); // close ajax
        }
    </script>
@endsection
