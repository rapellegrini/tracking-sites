<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Log;
use App\Repositories\Contracts\RequestHistoryRepositoryInterface;
use App\Repositories\Contracts\UrlRepositoryInterface;
use Carbon\Carbon;

class CheckStatus extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'checkstatus:check';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Check HTTP/code';

    private $requestHistoryRepository;
    private $urlRepository;

    public function __construct(RequestHistoryRepositoryInterface $requestHistoryRepository,
                                UrlRepositoryInterface $urlRepository)
    {
        parent::__construct();
        $this->requestHistoryRepository = $requestHistoryRepository;
        $this->urlRepository = $urlRepository;
    }

    
    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $urls = $this->urlRepository->getAllUrls();
        foreach ($urls as $value) {
            //Log::info($value->id);
            $ch = curl_init();   
            $options = array(
                CURLOPT_URL            => $value->url,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_HEADER         => true,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_ENCODING       => "",
                CURLOPT_AUTOREFERER    => true,
                CURLOPT_CONNECTTIMEOUT => 120,
                CURLOPT_TIMEOUT        => 120,
                CURLOPT_MAXREDIRS      => 10,
            );
            curl_setopt_array( $ch, $options );
            $response = curl_exec($ch); 
            $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);

            $dataRequest = array();
            $dataRequest['url_id'] = $value->id;
            $dataRequest['status_code'] = $httpCode;
            $dataRequest['ts_query'] = Carbon::now('America/Sao_Paulo');
            $register = $this->requestHistoryRepository->store($dataRequest);
            
            curl_close($ch);
        }
    }
}
