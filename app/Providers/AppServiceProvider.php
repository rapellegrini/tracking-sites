<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

use App\Repositories\Contracts\UrlRepositoryInterface;
use App\Repositories\Core\Eloquent\UrlRepository;
use App\Repositories\Contracts\RequestHistoryRepositoryInterface;
use App\Repositories\Core\Eloquent\RequestHistoryRepository;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(
            UrlRepositoryInterface::class,
            UrlRepository::class
        );

        $this->app->bind(
            RequestHistoryRepositoryInterface::class,
            RequestHistoryRepository::class
        );
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
