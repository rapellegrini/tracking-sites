<?php

namespace App\Repositories\Core\Eloquent;

use App\Model\RequestHistory;
use App\Repositories\Core\BaseEloquentRepository;
use App\Repositories\Contracts\RequestHistoryRepositoryInterface;


class RequestHistoryRepository extends BaseEloquentRepository implements RequestHistoryRepositoryInterface
{
    public function entity()
    {
        return RequestHistory::class;
    }

    public function responseLog($url_id)
    {
        $result = $this->entity::where('url_id', $url_id)
        ->select (
            'id',
            'status_code',
            'ts_query'
            )
        ->orderby('ts_query', 'DESC')
        ->get();

        return $result;

        
    }

        
}
