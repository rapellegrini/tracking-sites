<?php

namespace App\Repositories\Core\Eloquent;

use App\Model\Url;
use App\Repositories\Core\BaseEloquentRepository;
use App\Repositories\Contracts\UrlRepositoryInterface;
use Illuminate\Support\Facades\Auth;


class UrlRepository extends BaseEloquentRepository implements UrlRepositoryInterface
{
    public function entity()
    {
        return url::class;
    }

    public function getUrls()
    {
        $result = $this->entity::where('user_id', Auth::user()->id)
        //->whereNull('deactivation_date')
        ->select (
            'id',
            'url'
            )
        ->get();

        return $result;
    }

    public function getAllUrls()
    {
        $result = $this->entity::select (
            'id',
            'url'
            )
        ->get();

        return $result;
    }

   /* public function isRegistered($bed_code, $sector_id)
    {
        $bed = Bed::where('bed_code', $bed_code)
        ->where('sector_id', $sector_id)
        ->get(); 

        return $bed;
    }

    public function getBeds()
    {
        $result = $this->entity::join('sectors','sectors.id','beds.sector_id')
        //->where('sector_unities.id_hospital',$hospital_id)
        ->select (
            'beds.id',
            'beds.name',
            'sectors.name as sector_name',
            'beds.activation_date',
            'beds.deactivation_date'
            )
        ->get();

        return $result;

    }*/
 
        
}
