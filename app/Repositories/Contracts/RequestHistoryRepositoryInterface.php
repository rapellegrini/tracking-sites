<?php

namespace App\Repositories\Contracts;

interface RequestHistoryRepositoryInterface
{
    public function responseLog($url_id);
}