<?php

namespace App\Repositories\Contracts;

interface UrlRepositoryInterface
{
    public function getUrls();
    public function getAllUrls();
}