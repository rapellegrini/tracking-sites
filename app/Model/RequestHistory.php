<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class RequestHistory extends Model
{
    protected $fillable = [
        "url_id",
        "request",
        "status_code",
        "ts_query"
    ];
}
