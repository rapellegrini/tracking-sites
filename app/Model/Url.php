<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Url extends Model
{
    protected $fillable = [
        "url",
        "user_id"
    ];

    public function user() {
         return $this->belongsTo('App\user', 'user_id'); 
    }

}