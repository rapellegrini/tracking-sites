<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\Contracts\RequestHistoryRepositoryInterface;
use App\Repositories\Contracts\UrlRepositoryInterface;
use Carbon\Carbon;

class RequestHistoryController extends Controller
{
    private $requestHistoryRepository;
    private $urlRepository;

    public function __construct(RequestHistoryRepositoryInterface $requestHistoryRepository,
                                UrlRepositoryInterface $urlRepository)
    {
        $this->requestHistoryRepository = $requestHistoryRepository;
        $this->urlRepository = $urlRepository;
    }

    public function index($id)
    {
        $requests = $this->requestHistoryRepository->responseLog($id);
        return response()->json(['data' => $requests, 'status' => true]);
    }

}
