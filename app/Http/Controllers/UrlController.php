<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Repositories\Contracts\UrlRepositoryInterface;
use App\Http\Requests\UrlRequest;

class UrlController extends Controller
{
    private $urlRepository;

    public function __construct(UrlRepositoryInterface $urlRepository)
    {
        //$this->middleware("auth");
        $this->urlRepository = $urlRepository;
    }

    public function index() 
    {
        //$motorists = $this->motoristRepository->findAllWithPagination('name');
        //return view('motorist.list', compact('motorists'));

        $urls = $this->urlRepository->getUrls();
        return view('url.index', compact('urls'));
    }
    

    public function create() 
    {
        return view('url.create');
    }

    public function store(UrlRequest $request) 
    {
        $data = [
            'user_id' => Auth::user()->id,
            'url' => $request->url,
        ];
        $insertNew = $this->urlRepository->store($data);

        if ($insertNew)
            return redirect()
                     ->route('url.create')
                     ->with('success', 'Cadastro realizado com sucesso!');

        return redirect()
                    ->back()
                    ->with('error', 'Falha ao tentar cadastrar!');

    }

    public function historyRequest($id)
    {
        $url = $this->urlRepository->findById($id);
        return view('url.answers_requests', compact('id','url'));
    }

}
