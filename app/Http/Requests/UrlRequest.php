<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UrlRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'url' => 'required|url',
        ];
    }

     public function messages()
    {
        return [
            'url.required'  => 'Favor informar URL do site!',
            'url.url'  => 'Favor informar corretamenta a URL!',
        ];
    }
}
